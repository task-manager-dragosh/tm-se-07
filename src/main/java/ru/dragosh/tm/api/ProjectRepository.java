package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.util.List;

public interface ProjectRepository extends Repository<Project> {
    List<Project> findAll(String userId);
    Project find(String projectName, String userId);
    void removeAll(String userId);
}