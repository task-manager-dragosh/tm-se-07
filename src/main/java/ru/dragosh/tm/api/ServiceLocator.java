package ru.dragosh.tm.api;

import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;

import java.util.Map;

public interface ServiceLocator {
    Map<String, AbstractCommand> getCommands();
    ProjectService getProjectService();
    TaskService getTaskService();
    UserService getUserService();
    User getCurrentUser();
    void setCurrentUser(User user);
}
