package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.text.ParseException;
import java.util.List;

public interface ProjectService {
    List<Project> findAll(String userId);
    Project find(String projectName, String userId);
    void persist(Project project) throws ParseException;
    void merge(Project project);
    void remove(String projectId);
    void removeAll(String userId);
}