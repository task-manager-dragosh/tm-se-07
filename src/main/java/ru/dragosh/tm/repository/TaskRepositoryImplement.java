package ru.dragosh.tm.repository;

import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.api.TaskRepository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class TaskRepositoryImplement extends AbstractRepository<Task> implements TaskRepository {
    @Override
    public List<Task> findAll(final String projectId) {
        return base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task find(final String projectId, final String taskName) {
        return base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getName().equals(taskName))
                .findFirst().orElse(null);
    }

    @Override
    public void removeAll(final String projectId) {
        base.values().stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> base.remove(task.getId()));
    }
}
