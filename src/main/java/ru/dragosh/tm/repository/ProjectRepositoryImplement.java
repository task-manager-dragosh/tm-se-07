package ru.dragosh.tm.repository;

import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class ProjectRepositoryImplement extends AbstractRepository<Project> implements ProjectRepository {
    @Override
    public List<Project> findAll(final String userId) {
        return base.values().stream()
                .filter(project -> project.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public Project find(final String projectName, final String userId) {
        return base.values().stream()
                .filter(project -> project.getName().equals(projectName) && project.getUserId().equals(userId)).
                        findFirst().orElse(null);
    }

    @Override
    public void removeAll(final String userId) {
        base.values().stream()
                .filter(project -> project.getUserId().equals(userId))
                .forEach(project -> base.remove(project.getId()));
    }
}

