package ru.dragosh.tm.repository;

import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<E extends Entity> implements Repository<E> {
    protected final Map<String, E> base = new LinkedHashMap<>();

    @Override
    public void persist(E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void merge(E entity) {
        base.put(entity.getId(), entity);
    }

    @Override
    public void remove(String entityId) {
        base.remove(entityId);
    }
}
