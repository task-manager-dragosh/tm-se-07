package ru.dragosh.tm.repository;

import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.entity.User;

import java.util.LinkedHashMap;
import java.util.Map;

public final class UserRepositoryImplement implements UserRepository {
    private final Map<String, User> users = new LinkedHashMap<>();

    @Override
    public User find(String login, String password) {
        return users.values().stream()
                .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(String login) {
        return users.values().stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public void persist(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void merge(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void remove(String userId) {
        users.remove(userId);
    }
}
