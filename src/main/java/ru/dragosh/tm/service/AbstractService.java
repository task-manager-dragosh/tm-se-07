package ru.dragosh.tm.service;

import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public abstract class AbstractService<E extends Entity, R extends Repository<E>>{
    protected R repository;
    protected AbstractService(R repository) {
        this.repository = repository;
    }

    public void persist(final E entity) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateStart())));
        repository.persist(entity);
    }

    public void merge(final E entity) {
        repository.merge(entity);
    }

    public void remove(final String entityId) {
        repository.remove(entityId);
    }
}
