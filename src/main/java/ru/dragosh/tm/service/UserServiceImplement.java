package ru.dragosh.tm.service;

import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;

public final class UserServiceImplement implements UserService {
    private final UserRepository userRepository;

    public UserServiceImplement(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User find(String login, String password) {
        return userRepository.find(login, password);
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void persist(User user) {
        userRepository.persist(user);
    }

    @Override
    public void merge(User user) {
        userRepository.merge(user);
    }

    @Override
    public void remove(String userId) {
        userRepository.remove(userId);
    }
}
