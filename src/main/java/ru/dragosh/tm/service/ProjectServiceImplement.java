package ru.dragosh.tm.service;

import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public final class ProjectServiceImplement extends AbstractService<Project, ProjectRepository> implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImplement(final ProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public Project find(final String projectName, final String userId) {
        return projectRepository.find(projectName, userId);
    }

    @Override
    public void removeAll(final String userId) {
        projectRepository.removeAll(userId);
    }
}
