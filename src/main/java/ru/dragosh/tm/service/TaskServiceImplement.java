package ru.dragosh.tm.service;

import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class TaskServiceImplement extends AbstractService<Task, TaskRepository> implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImplement(final TaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String projectId) {
        return taskRepository.findAll(projectId);
    }

    @Override
    public Task find(final String projectId, final String nameTask) {
        return taskRepository.find(projectId, nameTask);
    }

    @Override
    public void removeAll(final String projectId) {
        if (projectId == null || projectId.isEmpty()) {
            log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        taskRepository.removeAll(projectId);
    }
}
