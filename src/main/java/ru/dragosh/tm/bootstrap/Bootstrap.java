package ru.dragosh.tm.bootstrap;

import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.command.app.AboutCommand;
import ru.dragosh.tm.command.app.ExitCommand;
import ru.dragosh.tm.command.app.HelpCommand;
import ru.dragosh.tm.command.project.*;
import ru.dragosh.tm.command.task.*;
import ru.dragosh.tm.command.user.*;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.MessageType;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.repository.UserRepositoryImplement;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.TaskServiceImplement;
import ru.dragosh.tm.service.UserServiceImplement;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {
    private ProjectService projectService = new ProjectServiceImplement(new ProjectRepositoryImplement());
    private TaskService taskService = new TaskServiceImplement(new TaskRepositoryImplement());
    private UserService userService = new UserServiceImplement(new UserRepositoryImplement());

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private User currentUser = new User("","", RoleType.INCOGNITO);

    public void init() {
        prepareCommands();
        prepareUsers();
        start();
    }

    private void start() {
        ConsoleUtil.log(MessageType.WELCOME_MESSAGE);
        ConsoleUtil.log(MessageType.HELP_MESSAGE);
        while (true) {
            String stringCommand = ConsoleUtil.readCommand().toLowerCase();
            AbstractCommand command = commands.get(stringCommand);

            if (command == null) {
                ConsoleUtil.log(MessageType.WRONG_COMMAND);
                continue;
            }
            if (!hasAccess(command)) {
                ConsoleUtil.log(MessageType.PERMISSION_DENIED);
                continue;
            }
            command.execute();
        }
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return this.commands;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(final User user) {
        this.currentUser = user;
    }

    private void prepareCommands() {
        List<AbstractCommand> commandList = new ArrayList<>();
        commandList.add(new PersistProjectCommand(this));
        commandList.add(new PersistTaskCommand(this));
        commandList.add(new FindProjectCommand(this));
        commandList.add(new FindAllProjectsCommand(this));
        commandList.add(new FindTaskCommand(this));
        commandList.add(new FindAllTasksCommand(this));
        commandList.add(new MergeProjectCommand(this));
        commandList.add(new MergeTaskCommand(this));
        commandList.add(new RemoveProjectCommand(this));
        commandList.add(new RemoveAllProjectsCommand(this));
        commandList.add(new RemoveTaskCommand(this));
        commandList.add(new RemoveAllTasksCommand(this));
        commandList.add(new HelpCommand(this));
        commandList.add(new ExitCommand(this));
        commandList.add(new AuthorisationCommand(this));
        commandList.add(new LogoutCommand(this));
        commandList.add(new RegistrationCommand(this));
        commandList.add(new ShowProfileCommand(this));
        commandList.add(new UpdateProfileCommand(this));
        commandList.add(new AboutCommand(this));
        commandList.forEach(command -> commands.put(command.getName(), command));
    }

    private boolean hasAccess(final AbstractCommand command) {
        return command.getRoles().contains(currentUser.getRole());
    }

    private void prepareUsers() {
        userService.persist(new User("root", "root", RoleType.ADMIN));
        userService.persist(new User("user", "user", RoleType.USER));
    }
}
