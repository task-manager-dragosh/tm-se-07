package ru.dragosh.tm.entity;

import java.util.*;

public final class Project implements Entity {
    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private String dateStart;
    private String dateFinish;
    private String userId;

    public Project(String name, String description, String dateStart, String dateFinish, String userId) {
        this.name = name;
        this.description = description;
        this.setDateStart(dateStart);
        this.setDateFinish(dateFinish);
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String projectName) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UUID проекта: " + this.id + ";\n" +
                "Название проекта: " + this.name + ";\n" +
                "Описание проекта: " + this.description + ";\n" +
                "Дата начала проекта: " + this.dateStart + ";\n" +
                "Дата окончания проекта: " + this.dateFinish + ";";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;

        if (id == null || name == null)
            return false;

        return name.equals(project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateStart, dateFinish);
    }
}