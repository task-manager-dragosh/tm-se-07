package ru.dragosh.tm.command;

import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.Scanner;
import java.util.Set;

public abstract class AbstractCommand {
    protected final ServiceLocator serviceLocator;
    protected final ProjectService projectService;
    protected final TaskService taskService;

    public AbstractCommand(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();
    public abstract Set<RoleType> getRoles();

    public String readWord(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
