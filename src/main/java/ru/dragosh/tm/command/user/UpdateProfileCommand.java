package ru.dragosh.tm.command.user;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.MessageType;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class UpdateProfileCommand extends AbstractCommand {
    public UpdateProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "update profile";
    }

    @Override
    public String getDescription() {
        return "(изменение данных профиля текущего пользователя)";
    }

    @Override
    public void execute() {
        System.out.println("1 - Изменить логин\n" + "2 - Изменить пароль\n" + "3 - Выйти");
        String command = new Scanner(System.in).nextLine();
        switch (command) {
            case "1": {
                changeLogin();
                break;
            }
            case "2": {
                changePassword();
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }

    private void changeLogin() {
        String newLogin = readWord("Введите новый логин: ");

        if (newLogin != null || newLogin.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (serviceLocator.getUserService().findByLogin(newLogin) != null) {
            ConsoleUtil.log(MessageType.USER_EXISTS);
            return;
        }

        User user = serviceLocator.getCurrentUser();
        user.setLogin(newLogin);
        serviceLocator.getUserService().merge(user);
    }

    private void changePassword() {
        String newHashPassword = ConsoleUtil.getHash(readWord("Введите новый пароль: "));
        if (newHashPassword == null || newHashPassword.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        User user = serviceLocator.getCurrentUser();
        user.setPassword(newHashPassword);
    }
}
