package ru.dragosh.tm.command.user;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.MessageType;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.HashSet;
import java.util.Set;

public class RegistrationCommand extends AbstractCommand {
    public RegistrationCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "register";
    }

    @Override
    public String getDescription() {
        return "(регистрация нового пользователя)";
    }

    @Override
    public void execute() {
        String login = readWord("Введите ваш логин: ");
        String password = readWord("Введите ваш пароль: ");
        if (login == null || login.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        if (password == null || password.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        if (serviceLocator.getUserService().findByLogin(login) != null) {
            ConsoleUtil.log(MessageType.USER_EXISTS);
            return;
        }

        User user = new User(login, password, RoleType.USER);
        serviceLocator.getUserService().persist(user);
        serviceLocator.setCurrentUser(user);
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
        }};
    }
}
