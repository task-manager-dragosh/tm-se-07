package ru.dragosh.tm.command.project;

import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.HashSet;
import java.util.Set;

public final class RemoveAllProjectsCommand extends AbstractCommand {
    public RemoveAllProjectsCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "remove all projects";
    }

    @Override
    public String getDescription() {
        return "(удаляет все проекты и связанные с ними задачи из баз данных Projects и Tasks)";
    }

    @Override
    public void execute() {
        projectService.removeAll(serviceLocator.getCurrentUser().getId());
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}