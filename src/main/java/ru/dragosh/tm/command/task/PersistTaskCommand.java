package ru.dragosh.tm.command.task;

import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public final class PersistTaskCommand extends AbstractCommand {
    public PersistTaskCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "add task";
    }

    @Override
    public String getDescription() {
        return "(добавляет в проект новую задачу)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");
        String taskDescription = readWord("Введите описание задачи: ");
        String taskDateStart = readWord("Введите дату начала выполнения задачи: ");
        String taskDateFinish = readWord("Введите дату окончания выполнения задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDescription == null || taskDescription.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateStart == null || taskDateStart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateFinish == null || taskDateFinish.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }

        Project project = projectService.find(projectName, serviceLocator.getCurrentUser().getId());
        if (project == null) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskService.find(project.getId(), taskName) != null) {
            ConsoleUtil.log(MessageType.TASK_EXISTS);
            return;
        }
        Task task = new Task(taskName, taskDescription, taskDateStart, taskDateFinish, project.getId(), serviceLocator.getCurrentUser().getId());
        try {
            taskService.persist(task);
        } catch (ParseException e) {
            log(MessageType.WRONG_DATE_FORMAT);
        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.ADMIN);
            add(RoleType.USER);
        }};
    }
}