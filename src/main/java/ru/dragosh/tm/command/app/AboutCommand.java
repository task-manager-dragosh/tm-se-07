package ru.dragosh.tm.command.app;

import com.jcabi.manifests.Manifests;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.RoleType;

import java.util.HashSet;
import java.util.Set;

public class AboutCommand extends AbstractCommand {
    public AboutCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "(вывод информации о сборке)";
    }

    @Override
    public void execute() {
        try {
            System.out.println("JAR was created by " + Manifests.read("Developer")
                    + " " + Manifests.read("AppVersion"));
        } catch (IllegalArgumentException ex) {

        }
    }

    @Override
    public Set<RoleType> getRoles() {
        return new HashSet<RoleType>() {{
            add(RoleType.INCOGNITO);
            add(RoleType.USER);
            add(RoleType.ADMIN);
        }};
    }
}
